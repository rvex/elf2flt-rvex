
#ifndef _ELF_RVEX_H
#define _ELF_RVEX_H

#include "elf/reloc-macros.h"

/* Creating indices for reloc_map_index array.  */
START_RELOC_NUMBERS(elf_rvex_reloc_type)
  RELOC_NUMBER (R_RVEX_NONE,			0)
  RELOC_NUMBER (R_RVEX_BRANCH,			1)
  RELOC_NUMBER (R_RVEX_MOV,			2)
  RELOC_NUMBER (R_RVEX_MOV_H,			3)
  RELOC_NUMBER (R_RVEX_MOV_L,			4)
  RELOC_NUMBER (R_RVEX_32,			5)
END_RELOC_NUMBERS(R_RVEX_MAX)
        
#endif /* _ELF_RVEX_H */
